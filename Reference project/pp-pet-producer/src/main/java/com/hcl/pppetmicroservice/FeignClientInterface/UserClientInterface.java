package com.hcl.pppetmicroservice.FeignClientInterface;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.hcl.pppetmicroservice.dto.UserDto;
import com.hcl.pppetmicroservice.exception.PetPeersException;

@FeignClient(name = "USER-SERVICE")

public interface UserClientInterface {
	@GetMapping("/user/{userId}")
	public UserDto getUserDetailByUserId(@PathVariable("userId") Long userId) throws PetPeersException;

}
