package com.hcl.pppetmicroservice.service;


import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.pppetmicroservice.exception.PetPeersException;
import com.hcl.pppetmicroservice.model.Pet;
import com.hcl.pppetmicroservice.repository.PetRepository;
import com.hcl.pppetmicroservice.validator.PetValidator;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	PetRepository petRepository;

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public Pet savePet(PetValidator petRequest) throws PetPeersException {
		Pet petCreated = null;
		ModelMapper modelMapper = new ModelMapper();
		Pet petRequested = modelMapper.map(petRequest, Pet.class);
		petCreated = petRepository.save(petRequested);
		if (petCreated != null) {
			return petCreated;
		} else {
			throw new PetPeersException("Pet Not created");
		}
	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public Set<Pet> getAllPets() throws PetPeersException {
		Set<Pet> pets = (Set<Pet>) petRepository.findAll();
		if (pets != null && pets.size() > 0) {
			return pets;
		} else {
			throw new PetPeersException("No Pets Available");
		}
	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public Optional<Pet> getPetByPetId(Long petId) throws PetPeersException {
		Optional<Pet> pet = petRepository.findById(petId);
		if (pet.isPresent()) {
			return pet;
		} else {
			throw new PetPeersException("No pet exists with given pet id");

		}

	}

	@Override
	public Set<Pet> getPetByUserId(Long userId) throws PetPeersException {
		Set<Pet> pets = (Set<Pet>) petRepository.findByUserId(userId);
		if (pets != null && pets.size() > 0) {
			return (Set<Pet>) pets;
		} else {
			throw new PetPeersException("You Don't have pets go buy some");
		}
	}

}

