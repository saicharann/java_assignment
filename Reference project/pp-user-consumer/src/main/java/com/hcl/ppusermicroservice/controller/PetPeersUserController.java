package com.hcl.ppusermicroservice.controller;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ppusermicroservice.dto.PetDto;
import com.hcl.ppusermicroservice.exception.PetPeersException;
import com.hcl.ppusermicroservice.model.User;
import com.hcl.ppusermicroservice.service.UserService;
import com.hcl.ppusermicroservice.validator.LoginValidator;
import com.hcl.ppusermicroservice.validator.UserValidator;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user")
@Slf4j
public class PetPeersUserController {
	private static final Logger logger = LogManager.getLogger(PetPeersUserController.class);
	@Autowired
	private UserService userService;

	@PostMapping("/add")
	public ResponseEntity<User> addUser(@Valid @RequestBody UserValidator userRequest) throws PetPeersException {
		User user = null;
		ResponseEntity<User> responseEntity = null;
		try {
			user = userService.addUser(userRequest);
			if (user != null) {
				responseEntity = new ResponseEntity<User>(user, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<User>(user, HttpStatus.BAD_REQUEST);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@PostMapping("/login")
	public ResponseEntity<Set<PetDto>> loginUser(@Valid @RequestBody LoginValidator loginRequest) throws PetPeersException {
		Set<PetDto> pets = null;
		ResponseEntity<Set<PetDto>> responseEntity = null;
		try {
			pets = userService.loginUser(loginRequest);
			if (pets != null) {
				responseEntity = new ResponseEntity<Set<PetDto>>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Set<PetDto>>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Set<PetDto>>(HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@PutMapping("/buyPet/{userId}/{petId}")
	public ResponseEntity<Object> buyPet(
			@PathVariable @Min(value = 1, message = "Minimum entry must be One") Long userId,
			@PathVariable @Min(value = 1, message = "Minimum entry must be One") Long petId)
			throws PetPeersException {
		PetDto userBoughtPets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			userBoughtPets = userService.buyPet(userId, petId);
			if (userBoughtPets != null) {
				responseEntity = new ResponseEntity<Object>(userBoughtPets, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Object>(userBoughtPets, HttpStatus.CONFLICT);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}
	
	@GetMapping("/myPets/{userId}")
	public ResponseEntity<Set<PetDto>> myPets(
			@PathVariable @Min(value = 1, message = "Minimum entry must be One") Long userId)
			throws PetPeersException {
		Set<PetDto> pets = null;
		ResponseEntity<Set<PetDto>> responseEntity = null;
		try {
			pets = userService.getMyPets(userId);
			if (pets != null && pets.size() > 0) {
				responseEntity = new ResponseEntity<Set<PetDto>>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Set<PetDto>>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Set<PetDto>>(HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

}
