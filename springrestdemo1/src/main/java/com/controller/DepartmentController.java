package com.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.model.Department;

@RestController
public class DepartmentController 
{
	//handler
	@RequestMapping(value="one",method=RequestMethod.GET)   //read create 
	public String sayHello() 
	{
		return "Welcome to spring Rest";
		
	}
//each method in Rest api is called as resource
	@RequestMapping(value="two",method=RequestMethod.GET)
	public Department getDepartment()
	{
		return new Department(10, "Development", "KA");
	}
}
