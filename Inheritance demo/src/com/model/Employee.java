package com.model;

public class Employee {
	
	private int employeeNo;
	private String employeeName;
	private float salary;
	
	public Employee() {
		super();
		System.out.println("default constructor of parent class is called first");
	}
	
	public void hello() {
		System.out.println("Employee hello method");
		
	}
	public Employee(int employeeNo,String employeeName , float salary) {
		super();
		this.employeeNo = employeeNo;
		this.employeeName = employeeName;
		this.salary = salary;
		
		
	}
	public Employee(int employeeNo , String employeeName) {
		super();
		this.employeeNo = employeeNo;
		this.employeeName = employeeName;
		
	}
	
	public int getEmployeeNo() {
		return employeeNo;
	}
	
	

}
